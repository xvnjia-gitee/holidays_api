<?php
/**
 * 精简节假日api
 * @copyright tool.bitefu.net
 * @auther xiaogg@sina.cn
 */
error_reporting("E_ALL");ini_set("display_errors", 1);
$data=$_REQUEST;
if(empty($data['d'])){
    header("content-Type: text/html; charset=utf-8");exit('?d=');
}else{
    header("content-Type: application/json; charset=utf-8");
    include('include/dateapi.class.php');//创建实例
    $api= new dateapi();
    $check=$api->check($data['d']);
    if($check['status']==0)ajaxReturn($result);//校验日期合法性  可不校验
    $result= $api->getday($data['d']);
    ajaxReturn($result);
}
function ajaxReturn($array){
    $content=json_encode($array,JSON_UNESCAPED_UNICODE);
    if(empty($_GET['callback'])){
        echo $content;exit;
    }else{
        echo $_GET['callback']."(".$content.")";exit;
    }
}
?>